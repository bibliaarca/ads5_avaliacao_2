package appdispositivosmoveis.edu.ifsc.br.avaliacao2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.text.NumberFormat;
import java.text.ParseException;

public class MainActivity extends AppCompatActivity {


    //Propriedade NumberFormat, para formatação de dados porcentagem e monetários
    private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private static NumberFormat percentFormat  = NumberFormat.getPercentInstance();

    EditText editTextValorEmprestimo;
    EditText editTextValorTxJuro;
    EditText editTextMeses;
    EditText editTextJuros;

    // Propriedades do container SharePreferences e Editor
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;

    //Propriedades da classe para armazenar os parametros informados para simulação.
    protected int meses;
    protected double valorEmprestimo;
    protected double txJuros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextValorEmprestimo= (EditText) findViewById(R.id.editTextValorEmprestimo) ;

        editTextValorTxJuro= (EditText) findViewById(R.id.editTextValorTxJuro);
        editTextValorEmprestimo.addTextChangedListener(amountEditTextWatcher);

        editTextMeses= (EditText) findViewById(R.id.editTextMeses);


    }



    public void simularListView(View view) {
        Financiamento f= null;
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setGroupingUsed(true);
        nf.setMinimumFractionDigits(2);

        this.meses=  Integer.parseInt( this.editTextMeses.getText().toString());
        this.valorEmprestimo=0.0;
        this.txJuros=0.1;

        try {
            valorEmprestimo= currencyFormat.parse(editTextValorEmprestimo.getText().toString()).doubleValue() ;
            txJuros= percentFormat.parse(editTextValorTxJuro.getText().toString()).doubleValue();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Carregando os valores a serem passados para proxíma activity em um bundle
        Bundle params = new Bundle();
        params.putInt("meses",meses);
        params.putDouble("valorEmprestimo", valorEmprestimo);
        params.putDouble("txJuros",txJuros);

        //Definindo uma nova intenção para abrir a simulação
        Intent intent= new Intent(this,Main3Activity.class);

        //Carregnado o bundle construido na intent a ser transmitida na aplicação
        intent.putExtras(params);
        //Disparando a intent e inicilalizando uma nova ativity e ao mesmo tempo passando os parametros definidos.
        startActivity(intent);

    }


    private final TextWatcher amountEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
