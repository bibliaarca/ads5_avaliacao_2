package appdispositivosmoveis.edu.ifsc.br.avaliacao2;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.NumberFormat;

public class Main3Activity extends AppCompatActivity {

    private View listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);


        this.listView=findViewById(R.id.listar);

        Intent intent=getIntent();
        int meses=intent.getExtras().getInt("meses");
        double valorEmprestimo=intent.getExtras().getDouble("valorEmprestimo");
        double txJuros=intent.getExtras().getDouble("txJuros");

        Financiamento f=  new Financiamento( meses,valorEmprestimo,txJuros );

        String[] fromColumns = {ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};
        int[] toViews = {R.id.coluna1, R.id.coluna2, R.id.coluna3};

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.activity_main3, cursor, fromColumns, toViews, 0);

        ListView listView = getListView();
        listView.setAdapter(adapter);

/*        FinanciamentoAdapterListView adapter = new FinanciamentoAdapterListView(this,
                R.layout.template_list_parcela_financiamento,
                f.getParecelas());

        listView.setAdapter(adapter);*/
        double soma = 0;
        for (Parcela p: f.getParecelas()){
            soma+=p.getValorPrestacao();
        }

        TextView tvTotalPago=(TextView)findViewById(R.id.total);
        String valorTotalCurrency =NumberFormat.getCurrencyInstance().format(soma);
        tvTotalPago.setText(valorTotalCurrency);

    }
}
